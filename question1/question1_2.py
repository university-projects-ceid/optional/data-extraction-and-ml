""" Question 2 main module. """
# Project libs
from .question1_1 import Dataset
from .question1_1 import Question1_1
from .ml_models import LogRegModel, KMeansModel

import numpy as np 

class Question1_2(Question1_1):
    """ Question 1.2 main class. """
    def __init__(self, dataframe):
        super().__init__(dataframe)
    
    def drop_ph_column(self):
        """ Drop ph column from dataset. """
        self.dataframe = self.dataframe.drop(columns='pH')
    
    def reset_dataframe(self, df):
        """ Resets objects dataframe. """
        self.dataframe = df

    def set_ph_median_in_empty_rows(self):
        """ Set ph median in Ph empty rows. """
        ph_median = self._calculate_ph_median()
        self.dataframe = self.dataframe.fillna(value=ph_median)
    
    def use_regression_in_empty_rows(self):
        """ Calculate value of Nan rows with logistic regression """
        modelObject = LogRegModel(self.dataframe)
        # Return the trained model
        model = modelObject.get_trained_model()
        # Return ph prediction values
        y_pred = modelObject.get_ph_predictions(model)
        self.dataframe.loc[self.dataframe['pH'].isnull(), 'pH'] = y_pred

    def use_clustering_in_empty_rows(self):
        """ Calculate value of Nan rows with cluster average values and return new dataset. """
        # I will implement an EM algorithm (https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm)
        # First we ll add to all the empty rows the column median and then we ll maximize the log-likelihood of the 
        # expectation step with the clustering centroids.
        modelObject = KMeansModel(self.dataframe, 4)
        # Get index of columns with nan in ph column.
        nan_indexes = modelObject.get_ph_nan_indexes()
        # Set ph average column in empty rows.
        self.set_ph_median_in_empty_rows()
        modelObject.df = self.dataframe
        # Train model.
        model = modelObject.get_trained_model()
        # Initiate cluster average object with 0 as each cluster pH average
        cluster_avg = {cluster:0 for cluster in set(model.labels_)}
        # Add all ph values for each specific cluster
        for [index, item] in enumerate(model.labels_):
            cluster_avg[item] +=  modelObject.df.iloc[index]['pH']
        # Calculate cluster average
        for element in set(model.labels_):
            cluster_avg[element] = cluster_avg[element]/list(model.labels_).count(element)
        # Set cluster average in all previously empty rows.
        for [index, item] in enumerate(model.labels_):
            modelObject.df.iloc[index]['pH'] = cluster_avg[item]
            if (index >= len(nan_indexes)):
                break
        
        return modelObject.df


    def perform_elbow_method(self):
        """ Perform elbow method in order to find correct cluster value. """
        modelObject = KMeansModel(self.dataframe, 4)
        modelObject.perform_elbow_method()

    def _calculate_ph_median(self):
        """ Calculate ph median and return it. """
        median = self.dataframe['pH'].median(skipna=True)

        return median
