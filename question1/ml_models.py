""" Module responsilbe for ML models to predict PH values. """
# Python libs
from abc import ABC, abstractmethod
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt
import numpy as np

class MLModelClass(ABC):
    """ Main Abstract class for ML models for predicting PH. """
    def __init__(self, df):
        self.df = df
        self.train_data = self.df[~np.isnan(self.df['pH'])] if 'pH' in self.df.columns else None
        self.test_data = self.df[np.isnan(self.df['pH'])] if 'pH' in self.df.columns else None

    @abstractmethod
    def get_trained_model(self):
        """ Returns a trained model on trained data. """
        pass

    @abstractmethod
    def get_ph_predictions(self, model):
        """ Returns list of predictions of ph. """
        pass


class LogRegModel(MLModelClass):
    """ Class Responsible for Log Regression Rows Fill """
    def __init__(self, df):
        super().__init__(df)
    
    def get_trained_model(self):
        """ Returns a trained Logistic Regression model on trained data. """
        model = LinearRegression()
        model.fit(self.train_data.drop(columns='pH'), self.train_data['pH'])

        return model
    
    def get_ph_predictions(self, model):
        """ Returns list of predictions of ph. """
        y_pred = model.predict(self.test_data.drop(columns='pH'))

        return y_pred

class KMeansModel(MLModelClass):
    """ Class Responsible for Kmeans Clustering Rows Fill """
    def __init__(self, df, n_clusters):
        self.n_clusters = n_clusters
        super().__init__(df)
    
    def get_trained_model(self):
        """ Returns a trained Clustering model on trained data. """
        model = KMeans(n_clusters=self.n_clusters)
        model.fit(self.train_data.drop(columns='pH'), self.train_data['pH'])

        return model
    
    def get_ph_predictions(self, model):
        """ Returns list of predictions of ph. """
        y_pred = model.predict(self.test_data.drop(columns='pH'))

        return y_pred
    
    def get_ph_nan_indexes(self):
        """ Return a list of column indexes where ph values are nan. """
        return self.df['pH'].index[self.df['pH'].apply(np.isnan)]
 
    def perform_elbow_method(self):
        """ Perform elbow method in order to decide on the number of clusters. """
        # Check if dataset contains NaNs and return.
        if self.df.isnull().values.any():
            print('Dataset contains empty values and clustering cannot be performed.')
            return
        distortions = []
        K = range(2,15)
        for k in K:
            kmeanModel = KMeans(n_clusters=k)
            kmeanModel.fit(self.df)
            distortions.append(kmeanModel.inertia_)
        
        plt.figure(figsize=(16,8))
        plt.plot(K, distortions, 'bx-')
        plt.xlabel('k')
        plt.ylabel('Distortion')
        plt.title('The Elbow Method showing the optimal cluster number.')
        plt.show()
