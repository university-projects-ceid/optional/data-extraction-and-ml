""" Module calculating evaluation metrics. """
# Python libs.
import numpy as np
import pandas as pd
import yaml, os.path, csv
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, accuracy_score

class Evaluation():
    """ Class responsilble for model evaluation metrics """

    def __init__(self, y_pred, test_data):
        """ Class constructor """
        self.y_pred = y_pred
        self.test_data = test_data
        if os.path.isfile('./question1/config.yaml'):
            config_dict = yaml.safe_load(open('./question1/config.yaml', 'r'))
            self.csv_path = config_dict['results']['csv_path']
        else:
            print('No results csv.')

    def get_evaluation_metrics(self):
        """ Get evalutation metrics and print to console. """
        # Prediction Labels
        labels = list(set([*self.y_pred ,*list(self.test_data.index)]))

        # Evaluation Metrics
        self.accuracy = accuracy_score(self.test_data.index, self.y_pred)
        self.precision = precision_score(self.test_data.index, self.y_pred, average='weighted',zero_division=0)
        self.recall = recall_score(self.test_data.index, self.y_pred, average='weighted')
        self.f1_metric = f1_score(self.test_data.index, self.y_pred, average='weighted', zero_division=0)
        conf_matrix = confusion_matrix(self.test_data.index, self.y_pred, labels=labels)
        # Convert confusion matrix to dataframe in order to add extra columns and rows with labels.
        confusion_matrix_dataframe = pd.DataFrame(conf_matrix, index=labels, columns=labels)

        # Print Results For First Question.
        print(f'Accuracy= {self.accuracy}, precision= {self.precision}, recall = {self.recall} and f1= {self.f1_metric}')
        print(f'Confusion Matrix:\Rows are True values and Columns are Predicted Values.\n{confusion_matrix_dataframe}')
    
    def write_to_csv(self, message="No message given."):
        """ Write results to csv. """
        with open(self.csv_path, 'a', newline='') as csvfile:
            csv_writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow([message])
            csv_writer.writerow(['accuracy', 'precision', 'recall', 'f1_score'])
            csv_writer.writerow([self.accuracy, self.precision, self.recall, self.f1_metric])
            csv_writer.writerow(['-']*4)

    
