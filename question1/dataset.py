""" Main file for dataset functions. """
# Python libs.
import pandas as pd
import numpy as np
import yaml
import os.path
from sklearn.model_selection import train_test_split

class Dataset():
    """ Class responsible for dataset functions. """
    def __init__(self):
        """ Class constructor. """
        if os.path.isfile('./question1/config.yaml'):
            config_dict = yaml.safe_load(open('./question1/config.yaml', 'r'))
            self.csv_path = config_dict['dataset']['csv_path']
            self.test_size = config_dict['dataset']['test_size']
            self.random_state = config_dict['dataset']['random_state']
            self.index_column = config_dict['dataset']['index_column']
        else:
            print('No yaml file found!.')
            exit(1)

    def get_wine_dataset(self):
        """ Returns wines dataset as pandas dataframe. """
        data = pd.read_csv(self.csv_path, header=0, sep=',', index_col=self.index_column)
        
        return data

    def get_train_test_wine_data(self, df):
        """ Returns data for train and test on wine dataset. """
        train, test = train_test_split(df, test_size=self.test_size, random_state=self.random_state)

        return train, test
    
    @staticmethod
    def remove_percentage_of_ph(df):
        """ Returns dataset without 33% of ph column as NaN. """
        # Get 33% of dataset.
        items_to_delete = int(len(df)*0.33)
        # Replace those items with NaN.
        df.iloc[:items_to_delete, df.columns.get_loc('pH')] = np.NaN

        return df
