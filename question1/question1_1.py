""" Question 1 main module. """
# Python libs.
from sklearn import svm
import numpy as np
from sklearn.utils.class_weight import compute_class_weight
# Project files.
from .dataset import Dataset
from .evaluation import Evaluation

DATASET = Dataset()

class Question1_1():
    """ Question 1 class containing according methods. """
    def __init__(self, dataframe):
        """ Class constructor """
        self.dataframe = dataframe

    def fit_predict_model_without_params(self, message=None):
        """ Create a model and create predictions without any params. """
        print(f"{message}\n")
        # Get train test data.
        train_data, test_data  = DATASET.get_train_test_wine_data(self.dataframe)
        # Create model
        clf = svm.SVC()
        # Train model
        clf.fit(train_data, train_data.index)
        # Make predictions
        y_pred = clf.predict(test_data)
        # Evaluate Results.
        evaluation = Evaluation(y_pred, test_data)
        # Get evaluation results.
        evaluation.get_evaluation_metrics()
        # Write results to csv.
        evaluation.write_to_csv(message)

    def fit_predict_model_with_params(self, message=None):
        """ Create a model and create predictions with params. """
        print(f"{message}\n")
        # Get train test data.
        print(self.dataframe)
        train_data, test_data  = DATASET.get_train_test_wine_data(self.dataframe)
        """ Calculate sample weights. These weights are not currently used. """
        # class_weights = compute_class_weight('balanced', np.unique(test_data.index), test_data.index)
        # class_weight = dict(zip(list(np.unique(test_data.index)), class_weights))

        """
        Create model
        Class weight can have beneficial results in accuracy, but not in f1.
        That is because if we just predict all the values to be 5 and 6, we will have 
        a good accuracy, cause most professionals have rated wine quality with 5 or 6. 
        That is a classic example of overfitting.
        By givving small weights to classes, our model is not overfitting. 
        """
        clf = svm.SVC(kernel='linear', class_weight={5: 0.3, 6:0.5})
        # Train model
        clf.fit(train_data, train_data.index)
        # Make predictions
        y_pred = clf.predict(test_data)
        # Evaluate Results.
        evaluation = Evaluation(y_pred, test_data)
        # Get evaluation results.
        evaluation.get_evaluation_metrics()
        # Write results to csv.
        evaluation.write_to_csv(message)
