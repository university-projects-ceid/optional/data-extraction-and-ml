# Question 1 of Data Extraction School Project

## Requirements
For downloading the project dependencies you will need [pip](https://pypi.org/project/pip/) and python 3.7.x. A usefull tool for handling different python versions is [pyenv](https://github.com/pyenv/pyenv). Follow the links for more instrunctions on downloading the previous programs.

## Installation
First clone the repository.
```
git clone https://gitlab.com/university-projects-ceid/optional/data-extraction-and-ml.git
```
To download python dependencies you will need [pipenv](https://pipenv.readthedocs.io/en/latest/).
```
pip install pipenv
```
Navigate into the project folder and run the following command to download the dependencies.
```
cd eguardaroba
pipenv install --dev
```
To run the project using the pip enviroment run:
```
pipenv shell
python app.py
```

