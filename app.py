""" Main Module """
# Project libs
from question1.question1_1 import Question1_1
from question1.question1_2 import Question1_2
from question1.dataset import Dataset

if __name__ == '__main__':
    # Get dataset.
    dataset = Dataset()
    df = dataset.get_wine_dataset()
    # Question 1.1
    question1_1 = Question1_1(df)
    question1_1.fit_predict_model_without_params("Running SVM without any params...")
    question1_1.fit_predict_model_with_params("Running SVM.SVC with linear kernel and class weights...")
    # Question 1.2
    # Drop ph column
    question1_2 = Question1_2(df)
    question1_2.drop_ph_column()
    question1_2.fit_predict_model_with_params("QUESTION 1.2 Without PH Column...")
    # Set PH Median
    question1_2.reset_dataframe(Dataset.remove_percentage_of_ph(df))
    question1_2.set_ph_median_in_empty_rows()
    question1_2.fit_predict_model_with_params("QUESTION 1.2 With PH Median...")
    # Use Logistic Regression
    question1_2.reset_dataframe(Dataset.remove_percentage_of_ph(df))
    question1_2.use_regression_in_empty_rows()
    question1_2.fit_predict_model_with_params("QUESTION 1.2 Using Log Regression...")
    # Use Clustering
    question1_2.reset_dataframe(Dataset.remove_percentage_of_ph(df))
    new_dataset = question1_2.use_clustering_in_empty_rows()
    question1_2.reset_dataframe(new_dataset)
    question1_2.fit_predict_model_with_params("QUESTION 1.2 Using Clustering...")